import { describe, it } from 'mocha';
import { expect } from 'chai';
import { hex_to_rgb } from '../src/converter.js';
describe("HEX-to-RGB Converter", () => {
it("is a function", () => {
expect(hex_to_rgb ).to.be.an('function');
});
it("should convert RED value correctly", () => {
    expect(hex_to_rgb('#000000').r).to.equal(0);
    expect(hex_to_rgb('#ff0000').r).to.equal(255);
    expect(hex_to_rgb('#880000').r).to.equal(136);
    expect(hex_to_rgb('#880000').r).to.not.equal(0);
  });
it("should convert GREEN value correctly", () => {
    expect(hex_to_rgb('#000000').g).to.equal(0);
    expect(hex_to_rgb('#00ff00').g).to.equal(255);
    expect(hex_to_rgb('#008800').g).to.equal(136);
    expect(hex_to_rgb('#008800').g).to.not.equal(0);
});
it("should convert BLUE value correctly", () => {
    expect(hex_to_rgb('#000000').b).to.equal(0);
    expect(hex_to_rgb('#0000ff').b).to.equal(255);
    expect(hex_to_rgb('#000088').b).to.equal(136);
    expect(hex_to_rgb('#000088').b).to.not.equal(0);
});
it("should convert RGB to HEX correctly", () => {
    expect(hex_to_rgb('#000000')).to.eql({ r: 0, g: 0, b: 0 });
    expect(hex_to_rgb('#ffffff')).to.eql({ r: 255, g: 255, b: 255 });
    expect(hex_to_rgb('#ff8800')).to.eql({ r: 255, g: 136, b: 0 });
    expect(hex_to_rgb('#ff8800')).to.not.eql({ r: 0, g: 255, b: 0 });
});
});