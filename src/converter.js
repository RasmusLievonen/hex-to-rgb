/**
 * Hex to RGB conversion
 * @param {string} hex Hexadecimal color code (e.g., "#00ff00")
 * @return {object} Object containing the RGB components { r, g, b }
 */
export const hex_to_rgb = (hex) => {
    hex = hex.replace('#', '');

    // Parse hex to RGB components
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);

    // Return RGB components as an object
    return { r, g, b };
};