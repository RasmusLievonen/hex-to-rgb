import { Router} from "express";
import { hex_to_rgb } from './converter.js';

const routes = Router();

// Endpoint GET '{{api_url}}/'
routes.get('/', (req, res) => res.status(200).send("Welcome"));

// Endpoint GET /hex-to-rgb?hex=ffffff
routes.get('/hex-to-rgb', (req, res) => {
    const hex = req.query.hex;
    const RGB = hex_to_rgb(hex);
    res.status(200).send(RGB);
 });
export default routes