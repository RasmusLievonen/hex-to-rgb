import express from "express";
import { after, before, describe, it } from "mocha";
import { expect } from "chai";
import routes from "../src/routes.js";

const API_URL = "http://localhost:3000/api/v1";
const app = express();
/** @type {import('node:http').Server} */
let server;

describe("REST API routes", () => {
    before(() => {
        app.use('/api/v1', routes);
        server = app.listen(3000, () => {})
    });
    it("can get response", async () => {
        const response = await fetch(`${API_URL}/`);
        expect(response.ok).to.be.true; // fast check
    });
    it("should convert HEX to RGB correctly", async () => {
        const response = await fetch(`${API_URL}/hex-to-rgb?hex=ffffff`);
        const text = await response.text();
        const expected = {"r":255,"g":255,"b":255};
        const actual = JSON.parse(text);
        expect(actual).to.eql(expected);
    });
    after((done) => {
        server.close(() => done());
    });
});